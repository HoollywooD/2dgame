﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    private float speed;
    public float Speed { get { return speed; } set { speed = value; } }

    private SpriteRenderer sprite;

    private Vector3 direction;
    public Vector3 Direction { set { direction = value; } }

    private GameObject parent;
    public GameObject Parent { get { return parent; } set { parent = value; } }

    [SerializeField]
    private bool flyAnimation;
    public bool FlyAnimation { get { return flyAnimation; } set { flyAnimation = value; } }

    private Animator animator;
    private ChangeStateBullet ChangeState
    {
        get { return (ChangeStateBullet)animator.GetInteger("State"); }
        set { animator.SetInteger("State", (int)value); }
    }
    // Use this for initialization
    private void Awake()
    {
        animator = gameObject.GetComponent<Animator>();
        sprite = gameObject.GetComponentInChildren<SpriteRenderer>();
    }
    private void Start()
    {
        if (flyAnimation)
        {
            Destroy(gameObject, 1.8F);
            ChangeState = ChangeStateBullet.Fly;
        }
        else
        {
            ChangeState = ChangeStateBullet.Idle;
        }
            
    }
    private void Update()
    {
        if (flyAnimation)
            transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed * Time.deltaTime);
    }

    public virtual void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider)
        {
            if (flyAnimation && parent.tag != collider.gameObject.tag) Destroy(gameObject);
            if (!flyAnimation && collider.gameObject.tag == "Player")
            {
                Destroy(gameObject);
                Things.bullets++;
            }
        }
    }

}

enum ChangeStateBullet
{
    Idle,
    Fly
}