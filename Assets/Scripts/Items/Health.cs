﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : Items {
    public override void OnTriggerEnter2D(Collider2D collider)
    {
        base.OnTriggerEnter2D(collider);
        if (collider.gameObject.tag == "Player") Things.healths++;
    }
}
