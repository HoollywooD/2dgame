﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour {

    private bool IsGrounded;
    private Rigidbody2D rigidbody;

    void Awake()
    {
        rigidbody = gameObject.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        CheckGround();
        if (IsGrounded) rigidbody.AddForce(transform.up * 0.2F, ForceMode2D.Impulse);
    }

    void CheckGround()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.2F);
        IsGrounded = colliders.Length > 2;
    }
    public virtual void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player" || collider.gameObject.tag == "Bullet")
        {
            Destroy(gameObject);
        }
    }
}
