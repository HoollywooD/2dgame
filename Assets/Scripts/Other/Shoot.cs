﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    private Bullet bulletPrefab;
    private SpriteRenderer sprite;
    [SerializeField]
    private float minTime;
    [SerializeField]
    private float maxTime;
    [SerializeField]
    private Vector3 position;
    
    // Use this for initialization
    private void Awake()
    {
        
        bulletPrefab = Resources.Load<Bullet>("Bullet");
        sprite = gameObject.GetComponentInChildren<SpriteRenderer>();
    }

    void Start () {

    }

    private void FixedUpdate()
    {
        if (gameObject.tag == "Enemy")
        {
            float rnd = Random.Range(0, 10000);
            if (rnd >= minTime && rnd <= maxTime)
                Shooting();
        }
    }

    public void ShootChar()
    {

        Shooting();
    }

    private void Update()
    {

    }

    void Shooting()
    {
        Vector3 positionUnit = transform.position; positionUnit += position; position.x *= (sprite.flipX ? -1.0F : 1.0F);
        Bullet newBullet = Instantiate(bulletPrefab, positionUnit, bulletPrefab.transform.rotation) as Bullet;
        if (gameObject.GetComponent<Character>()) newBullet.Speed = 13.0F;
        else newBullet.Speed = 10.0F;
        newBullet.Parent = gameObject;
        newBullet.FlyAnimation = true;
        newBullet.Direction = newBullet.transform.right * (sprite.flipX ? -1.0F : 1.0F);
    }

}
