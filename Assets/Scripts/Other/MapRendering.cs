﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapRendering : MonoBehaviour {
    //Monsters
    private GameObject flyMonster;
    private GameObject flyMonsterShoot;
    private GameObject rockObstacles;
    private GameObject stayObstacles;
    private GameObject walkingMonster;
    private GameObject zombie;
    //Characters
    private GameObject character;
    //Items
    private GameObject health;
    private GameObject coin;
    private GameObject bullet;
    //Blocks
    private GameObject block1;

    private void Awake()
    {
        //Monsters
        flyMonster = Resources.Load<GameObject>("FlyMonster");
        flyMonsterShoot = Resources.Load<GameObject>("FlyMonsterShoot");
        rockObstacles = Resources.Load<GameObject>("RockObstacles");
        stayObstacles = Resources.Load<GameObject>("StayObstacles");
        walkingMonster = Resources.Load<GameObject>("WalkingMonster");
        zombie = Resources.Load<GameObject>("Zombie");
        //Character
        character = Resources.Load<GameObject>("Character");
        //Items
        health = Resources.Load<GameObject>("Health");
        coin = Resources.Load<GameObject>("Coin");
        bullet = Resources.Load<GameObject>("Bullet");
        block1 = Resources.Load<GameObject>("Block");
    }
    void Start () {
        for (int i = -20; i < 21; i++)
            Instantiate(block1, new Vector3(i, 0, 0), Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
