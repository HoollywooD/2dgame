﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    [SerializeField]
    private float speed = 3.0F;

    [SerializeField]
    private Transform target;
    // Use this for initialization
    void Awake () {
        if (!target) target = FindObjectOfType<Character>().transform;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 position = target.position; position.z = -10.0F; position.y += 3.5F;
        transform.position = Vector3.Lerp(transform.position, position, speed * Time.deltaTime);
	}
}
