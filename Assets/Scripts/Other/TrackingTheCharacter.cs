﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingTheCharacter : MonoBehaviour {
    [SerializeField]
    private Character character;

    private void Awake()
    {
        if (!character) character = FindObjectOfType<Character>();
    }

    void Update () {
        (gameObject.GetComponentInChildren<SpriteRenderer>()).flipX = (gameObject.transform.position.x - character.transform.position.x > 0) ? true : false;

    }
}
