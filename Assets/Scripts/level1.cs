﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class level1 : MonoBehaviour {
    bool pause;
    [SerializeField]
    GameObject MenuPause;
	// Use this for initialization
	void Start () {
        Things.healths = 5;
        Things.bullets = 0;
        Things.coins = 0;
        pause = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!pause)
            {
                OnPause();
            }
            else
            {
                OffPause();
            }
        }
	}
    
    public void OnPause()
    {
        Time.timeScale = 0;
        pause = true;
        MenuPause.SetActive(true);
    }

    public void OffPause()
    {
        Time.timeScale = 1;
        pause = false;
        MenuPause.SetActive(false);
    }
}
