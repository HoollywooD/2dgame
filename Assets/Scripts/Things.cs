﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Things : MonoBehaviour {

    public static int healths;
    public static int bullets;
    public static int coins;
    [SerializeField]
    Text textHealths;
    [SerializeField]
    Text textBullets;
    [SerializeField]
    Text textCoins;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        textHealths.text = Convert.ToString(healths);
        textBullets.text = Convert.ToString(bullets);
        textCoins.text = Convert.ToString(coins);
    }
}
