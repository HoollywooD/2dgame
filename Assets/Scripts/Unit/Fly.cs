﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fly : Monsters
{
    [SerializeField]
    private float speed = 3.0F;
    [SerializeField]
    private Vector3 endPosition;

    private SpriteRenderer sprite;
    [SerializeField]
    private float[] xBorder;
    [SerializeField]
    private float[] yBorder;

    private bool anim;

    void Start () {
        sprite = gameObject.GetComponentInChildren<SpriteRenderer>();
        ChangeCountPosition();
        anim = true;
    }

    private void FixedUpdate()
    {
        if (transform.position == endPosition && anim)
        {
            anim = false;
            StartCoroutine(Wait(Mathf.Floor(Random.Range(0, 3))));
        }
        if (anim)
        {
            transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
            if (!(transform.position == endPosition))
                sprite.flipX = (endPosition.x - transform.position.x) < 0.0F;
        }
    }

    void ChangeCountPosition()
    {
        endPosition = new Vector3(Random.Range(xBorder[0], xBorder[1]), Random.Range(yBorder[0], yBorder[1]), 0);
    }

    IEnumerator Wait(float time)
    {
        yield return new WaitForSeconds(time);
        ChangeCountPosition();
        anim = true;
    }
}

enum ChangeState
{
    Idle,
    Fly
}
