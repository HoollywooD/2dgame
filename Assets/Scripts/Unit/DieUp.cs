﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieUp : Monsters
{ 
    private BoxCollider2D boxColl2D;
    public BoxCollider2D BoxColl2D { get { return boxColl2D; } }
    private void Awake()
    {
        boxColl2D = gameObject.GetComponent<BoxCollider2D>();
    }
    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        if (unit)
            if (Mathf.Abs(gameObject.transform.position.x - unit.transform.position.x) < (boxColl2D.size.x) / 2)
                ReceiveDamage();
    }
}
