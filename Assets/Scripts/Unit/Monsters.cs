﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monsters : Unit {
    protected virtual void OnTriggerEnter2D(Collider2D collider)
    {
        Bullet bullet = collider.GetComponent<Bullet>();
        if (((bullet == null) ? false : (bullet.Parent.tag == "Player")))
            ReceiveDamage();
    }
}
