﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Moveable : Monsters {
    [SerializeField]
    private float speed;
    private SpriteRenderer sprite;
    private Vector3 direction;

    private void Awake()
    {
        sprite = gameObject.GetComponentInChildren<SpriteRenderer>();
        direction = new Vector3(((sprite.flipX) ? -1.0F: 1.0F), 0.0F, 0.0F);
    }
    private void FixedUpdate()
    {
        Vector2 sizeCollider = (gameObject.GetComponent<BoxCollider2D>()).size;
        Vector2 offsetCollider = (gameObject.GetComponent<BoxCollider2D>()).offset;

        Collider2D[] sideCollider = Physics2D.OverlapCircleAll(transform.position + transform.right * ((sprite.flipX) ? -1.0F : 1.0F) * (sizeCollider.x / 2) + transform.up , 0.1F);
        if (sideCollider.Length > 2 && sideCollider.All((x => !x.GetComponent<Character>())))
        {
            direction = new Vector3(((sprite.flipX) ? 1.0F : -1.0F), 0.0F, 0.0F);
            sprite.flipX = ((sprite.flipX) ? false : true);
        }
        else
        {
            Vector3 bottomPosition = gameObject.transform.position;
            bottomPosition.x += (sizeCollider.x / 2) * ((sprite.flipX) ? -1.0F : 1.0F) + 0.1F; bottomPosition.y -= 0.5F;
            Collider2D[] bottomColliders = Physics2D.OverlapCircleAll(bottomPosition, 0.4F);
            if (bottomColliders.Length == 0)
            {
                direction = new Vector3(((sprite.flipX) ? 1.0F : -1.0F), 0.0F, 0.0F);
                sprite.flipX = ((sprite.flipX) ? false : true);
            }
        }
        gameObject.transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed * Time.deltaTime);
    }

    protected override void OnTriggerEnter2D(Collider2D collider) {    }
}
