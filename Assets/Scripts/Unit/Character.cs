﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : Unit
{

    [SerializeField]
    private float speed = 0;
    [SerializeField]
    private int lives = 5;
    [SerializeField]
    private float jumpForce = 15.0F;
    private Vector3 direction;

    bool IsGrounded = false;
    private CharState State
    {
        get { return (CharState)animator.GetInteger("State"); }
        set { animator.SetInteger("State", (int)value); }
    }

    new private Rigidbody2D rigidbody;
    private Animator animator;
    private SpriteRenderer sprite;

    private Bullet bullet;
    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sprite = GetComponentInChildren<SpriteRenderer>();
        bullet = Resources.Load<Bullet>("Bullet");
    }

    private void FixedUpdate()
    {
        CheckGround();
    }

    public void LeftMove()
    {
        direction = transform.right * -1.0F;
        speed = 5.0f;
    }

    public void RightMove()
    {
        direction = transform.right * 1.0F;
        speed = 5.0f;
    }

    public void Stay()
    {
        speed = 0;
    }

    public void Jump()
    {
        if (IsGrounded)
            rigidbody.AddForce(transform.up* jumpForce, ForceMode2D.Impulse);
    }

    private void Update () {
        if (IsGrounded) State = CharState.Idle;
        if (speed > 0) Run();
    }

    void Run()
    {
        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed * Time.deltaTime);
        sprite.flipX = direction.x < 0.0F;
        if (IsGrounded) State = CharState.Run;
    }

    private Collider2D colliderObj;
    public override void ReceiveDamage()
    {
        lives--;
        rigidbody.velocity = Vector3.zero;
        rigidbody.AddForce(transform.right * 10.0F * ((colliderObj.gameObject.transform.position.x - transform.position.x < 0) ? 1.0F : -1.0F) +
                            transform.up * 8.0F * ((colliderObj.gameObject.transform.position.y - transform.position.y < 0) ? 1.0F : -1.0F), ForceMode2D.Impulse);
        Debug.Log(lives);
        colliderObj = null;
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        colliderObj = collider;
        Unit unit = collider.GetComponent<Unit>();
        Bullet bullet = collider.GetComponent<Bullet>();
        DieUp obstacklesAll = collider.GetComponent<DieUp>();
        if (bullet)
        {
            if (bullet.FlyAnimation && bullet.Parent.tag == "Enemy")
            {
                ReceiveDamage();
                Things.healths--;
            }
                
        }
        if (obstacklesAll)
            if (!(Mathf.Abs(gameObject.transform.position.x - unit.transform.position.x) < (obstacklesAll.BoxColl2D.size.x) / 2))
            {
                ReceiveDamage();
                Things.healths--;
            }
                
        if (unit && !obstacklesAll)
        {
            ReceiveDamage();
            Things.healths--;
        }
           
    }
    void CheckGround()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.4F);
        IsGrounded = colliders.Length > 1;
        if (!IsGrounded) State = CharState.Jump;
    }
}

public enum CharState
{
    Idle,
    Run,
    Jump
}