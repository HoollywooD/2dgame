﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyBird : MonoBehaviour {
    [SerializeField]
    private float[] xBorder;
    [SerializeField]
    private float[] yBorder;
    [SerializeField]
    private float speed;

    private SpriteRenderer sprite;
    private Vector3 endPosition;   
    private bool anim;

    private void Awake()
    {
        sprite = gameObject.GetComponentInChildren<SpriteRenderer>();
    }

    void Start () {
        endPosition = gameObject.transform.position;
        anim = true;
    }
    private void FixedUpdate()
    {
        if (transform.position == endPosition && anim)
        {
            anim = false;
            StartCoroutine(Wait(Mathf.Floor(Random.Range(0, 3))));
        }
        else if (anim)
        {
            transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
            if (!(transform.position == endPosition))
                sprite.flipX = (endPosition.x - transform.position.x) < 0.0F;
        }
    }

    void ChangeCountPosition()
    {
        endPosition = new Vector3(Random.Range(xBorder[0], xBorder[1]), Random.Range(yBorder[0], yBorder[1]), 83);
    }

    IEnumerator Wait(float time)
    {
        yield return new WaitForSeconds(time);
        ChangeCountPosition();
        anim = true;
    }
}
