﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {
    [SerializeField]
    private GameObject buttonsMainMenu;
    [SerializeField]
    private GameObject buttonsExit;

    [SerializeField]
    private GameObject speakerOn;
    [SerializeField]
    private GameObject speakerOff;
    [SerializeField]
    private GameObject audioListenerObj;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SpeakerOff()
    {
        AudioSource[] audioSources = GetComponents<AudioSource>();
        audioSources[0].enabled = false;
        audioSources[1].Pause();
        audioListenerObj.GetComponent<AudioListener>().enabled = false;
        speakerOff.SetActive(false);
        speakerOn.SetActive(true);
    }

    public void SpeakerOn()
    {
        AudioSource[] audioSources = GetComponents<AudioSource>();
        audioSources[0].enabled = true;
        audioSources[1].Play();
        audioListenerObj.GetComponent<AudioListener>().enabled = true;
        speakerOff.SetActive(true);
        speakerOn.SetActive(false);
    }

    public void ShowExitButtons()
    {
        GetComponent<AudioSource>().Play();
        buttonsMainMenu.SetActive(false);
        buttonsExit.SetActive(true);
    }
    public void ShowMainMenu()
    {
        GetComponent<AudioSource>().Play();
        buttonsMainMenu.SetActive(true);
        buttonsExit.SetActive(false);
    }
    public void ExitApp()
    {
        GetComponent<AudioSource>().Play();
        Application.Quit();
    }
    public void LoadGame()
    {
        GetComponent<AudioSource>().Play();
        SceneManager.LoadScene("level1");
    }
     public void LoadMainMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
